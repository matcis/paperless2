var pin = '';
$(document).ready(function() {
    setLogLevel(3);
    $("#sendPINbutton").prop('disabled', true);
    $('.pin-inputs-container input').keydown(function(e) {
        if (this.value) {
            this.value = '';
        }
    });

    $('.pin-inputs-container input').keyup(function(e) {
        if (this.value) {
            $(this).next().focus();
        }
        checkIfPinCanBeSend();
    });

    $("#sendPINbutton").click(function() {
        $("#sendPINbutton").prop('disabled', true);

        pin = '';
        $('.pin-inputs-container input').each(function(index) {
            pin += $(this).val();
        });

        loadFiles();
    });
});

function checkIfPinCanBeSend() {
    var areAllFieldsFilled = true;
    $('.pin-inputs-container input').each(function(index) {
        if (!$(this).val()) {
            areAllFieldsFilled = false;
        }
    });

    $("#sendPINbutton").prop('disabled', !areAllFieldsFilled);
}

function loadFiles() {
    downloadList({ 'channel': "Paperless", 'call': 'onListDownload', 'user': 'Paperless' });
}

function onListDownload(data) {
    var parameterJson = JSON.parse(data);
    var json = parameterJson['parameter'];
    var state = json['status'];
    if ("OK" == state) {
        var list = json.data;
        showFiles(list)
    }
}

function showFiles(files) {
    if (files != null) {
        for (var i in files) {
            addFile(files[i]);
        }
        $(".file-list-wrapper").removeClass("hide");
        $(".pin-box").addClass("hide");
    }
}


function addFile(file) {
    var li = document.createElement('li');
    $(li).text(file.fileName);
    $("#fileslist").append(li);

    $(li).click(function() {
        onFileClick(file);
    });
}

function onFileClick(file) {
    download_pdf(file)
}

var downloadId = -1;

function download_pdf(file) {

    //Problems with sending https request(?) in signatus
    // var newUrl = file.url.replace('https', 'http');
    var newUrl = file.url.substring(5);
    // // no possibility to pass argument to callback ? 
    downloadId = file.id;
    alert('http' + newUrl)
    download({
        'savePath': '/temp/downloaded.pdf',
        'url': 'http' + newUrl,
        'call': 'onDownload'
    });
}

function onDownload(data) {
    alert(data)
        //TODO pass id in a better way
    signDocument();
}

function signDocument() {
    signPdf({
        'path': '/temp/downloaded.pdf',
        'savePath': 'final/signed.pdf',
        'call': 'onSignDocument',
    });
}

function onSignDocument() {
    send({
        'file': 'final/signed.pdf',
        'channel': 'PaperlessUp',
        'call': 'onSend'
    });
}

function onSend(res) {
    alert(res);
    //TODO show success/failure view
}

function openWindow(url) {
    window.location.href = url;
}